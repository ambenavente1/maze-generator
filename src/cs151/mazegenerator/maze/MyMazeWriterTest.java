package cs151.mazegenerator.maze;

import java.io.FileNotFoundException;
import java.io.UnsupportedEncodingException;
import java.util.Scanner;

/**
 * Tests the maze writer
 *
 * @author Anthony Benavente
 * @version 12/8/13
 */
public class MyMazeWriterTest {

    /**
     * Tests the maze writer program
     *
     * @param args Not needed in this program
     */
    public static void main(String[] args) {
        MyMazeConfig config = new MyMazeConfig();
        Scanner in = new Scanner(System.in);

        System.out.println("Enter the width: ");
        config.setWidth(in.nextInt());
        System.out.println("Enter the height: ");
        config.setHeight(in.nextInt());

        // -------------------------
        //    Flush out the input
        // -------------------------
        in.nextLine();

        MyMaze maze = new MyMaze(config);

        MyMazeWriter writer = new MyMazeWriter(maze);
        try {
            System.out.println("Enter the name of the file: ");
            writer.writeMaze(in.nextLine());
            System.out.println("Maze successfully created!");
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } finally {
            in.close();
        }
    }

}