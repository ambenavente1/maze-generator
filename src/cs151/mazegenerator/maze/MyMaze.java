package cs151.mazegenerator.maze;

import cs151.mazegenerator.astar.AStar;
import cs151.mazegenerator.astar.Grid;

import java.awt.Point;
import java.util.ArrayList;
import java.util.Random;
import java.util.List;

/**
 * Represents a maze that is used for maze generation.  Currently, the width
 * and height of the maze are limited because of the recursive function.  On
 * my to-do list, I plan on changing it to an iterative algorithm.
 *
 * @author Anthony Benavente
 * @version 2/23/14
 */
public class MyMaze {

    /**
     * The random number generator for maze generation
     */
    private Random rand;

    /**
     * Width of the maze in tiles
     */
    private int width;

    /**
     * Height of the maze in tiles
     */
    private int height;

    /**
     * The maze represented by a 2d arrays of maze-cells
     */
    private MyMazeCell[][] cells;

    /**
     * Create a maze with the passed in configurations.
     * <em>
     * WARNING: The maximum area of the maze on my machine is 49,284 or
     * 222 x 222 due to restrictions with recursion.  This will be different
     * with different machines depending on the amount of virtual memory
     * allocated to the stack.
     * </em>
     *
     * @param mazeConfig configurations for the maze to create
     */
    public MyMaze(MyMazeConfig mazeConfig) {
        this.width = mazeConfig.getWidth();
        this.height = mazeConfig.getHeight();
        this.cells = new MyMazeCell[height][width];
        this.rand = new Random();

        // Generate the maze cells to be default at first
        for (int row = 0; row < height; row++) {
            for (int col = 0; col < width; col++) {
                cells[row][col] = new MyMazeCell(col, row, this);
            }
        }

        // Where the meat is
        generateMaze();
    }


    /**
     * Generates the maze itself.  Call this before writing the maze to a
     * file. (Should be called in the constructor)
     */
    private void generateMaze() {

        Point enter = new Point(0, 1);
        Point exit  = new Point(width - 2, height - 1);

        Grid grid = new Grid(width, height);
        AStar aStar = new AStar(grid);

        do {
            // TODO: Iterative algorithm instead of recursive
            // Start by calling the recursive evaluateCell method
            evaluateCell(new Point(1, 1));

            setEntrance(enter);
            setExit(exit);

        } while (aStar.calculatePath(enter, exit).size() == 0);
    }


    /**
     * Sets the tile at the given point to be the entrance to the maze
     *
     * @param point The point in the maze to make the entrance
     */
    private void setEntrance(Point point) {
        cells[point.y][point.x].setEntrance(true);
        cells[point.y][point.x].setWall(false);
    }

    /**
     * Sets the tile at the given point to be the exit of the maze
     *
     * @param point The point in the maze to make the exit
     */
    private void setExit(Point point) {
        cells[point.y][point.x].setExit(true);
        cells[point.y][point.x].setWall(false);
    }

    /**
     * Evaluates a cell and recursively checks until no cells have
     * any valid neighbors.  This method is an adaptation of a method
     * from the book <em>XNA 4 3D Game Development By Example Beginners
     * Guide</em> by Kurt Jaegers.
     *
     * @param cell Cell to evaluate
     */
    private void evaluateCell(Point cell) {
        List<Integer> neighbors = new ArrayList<Integer>(4);

        // There will be four neighbors for this cell
        for (int i = 0; i < 4; i++){
            neighbors.add(i);
        }

        // Keep repeating the evaluate process for all neighbors
        // of the current cell
        while (neighbors.size() > 0) {

            // This gets the index for the next neighbor to evaluate
            int selected = neighbors.get(rand.nextInt(neighbors.size()));
            neighbors.remove((Integer)selected);

            // This will change when the random number (selected) is used to
            // determine which neighbor to evaluate next
            Point neighbor;

            // The index selected from the array list of indexes determine
            // which neighbor to evaluate next
            switch (selected) {
                case 0:
                    neighbor = new Point(cell.x - 1, cell.y);
                    break;
                case 1:
                    neighbor = new Point(cell.x + 1, cell.y);
                    break;
                case 2:
                    neighbor = new Point(cell.x, cell.y - 1);
                    break;
                default:
                    // This should be "3" but you never know
                    neighbor = new Point(cell.x, cell.y + 1);
                    break;
            }

            // Only evaluate the cell if the selected neighbor is actually in
            // the maze and is valid.  See the method "isValid" to see what
            // constitutes as **valid**
            if (isInBounds(neighbor)) {
                if (isValid(neighbor, cell)) {
                    cells[neighbor.y][neighbor.x].setWall(false);
                    cells[neighbor.y][neighbor.x].setVisited(true);
                    cells[cell.y][cell.x].setWall(false);
                    evaluateCell(neighbor);
                }
            }
        }
    }

    /**
     * If the point is in the array
     *
     * @param p The point to check
     * @return If the point is in the array
     */
    public boolean isInBounds(Point p) {
        return p.x > 0 && p.x < width - 1 && p.y > 0 && p.y < height - 1;
    }

    /**
     * If the cell has not been visited and has no opening
     *
     * @param cell     The owner cell
     * @param neighbor The cell neighboring the owner cell
     * @return If the cell is valid meaning if the cell has not been visited
     * and the cells neighboring it are not open cells
     */
    public boolean isValid(Point cell, Point neighbor) {
        return !cells[cell.y][cell.x].isVisited()
                && !cells[cell.y][cell.x].hasOpening(neighbor);
    }

    /**
     * Gets if the point passed in returns a cell in the maze that is a wall
     *
     * @param p The point to check
     * @return if the cell at the point is a wall cell
     */
    public boolean isWall(Point p) {
        boolean isWall = false;
        if ((p.y >= 0) && (p.y < cells.length)
         && (p.x >= 0) && (p.x < cells[p.y].length)) {
            isWall = cells[p.y][p.x].isWall();
        }
        return isWall;
    }

    /**
     * Gets the width of the maze by the number of tiles
     *
     * @return the number of tiles wide this maze is
     */
    public int getWidth() {
        return width;
    }

    /**
     * Gets the height of the maze by the number of tiles
     *
     * @return the number of tiles tall this maze is
     */
    public int getHeight() {
        return height;
    }

    /**
     * Gets the cell at a given column (x) and row (y).  Column and row can
     * also be viewed as (x, y) coordinates where col is the x and row is
     * the y. If x or y are not in bounds in the maze, the function returns
     * null.
     *
     * @param x The column to get the cell from
     * @param y The row to get the cell from
     * @return
     */
    public MyMazeCell getCell(int x, int y) {
        MyMazeCell result = null;
        if (x >= 0 && x < width &&
            y >= 0 && y < height) {
            result = cells[y][x];
        }
        return result;
    }
}
