package cs151.mazegenerator.maze;

import java.awt.*;
import java.util.*;
import java.util.List;

/**
 * Used to split the maze into a square grid. Each square in the grid is a
 * maze.MyMazeCell object.
 *
 * @author Anthony Benavente
 * @version 2/23/14
 */
public class MyMazeCell {

    /**
     * Pointer to the parentMaze of this maze
     */
    private MyMaze parentMaze;

    /**
     * The x coordinate of the cell
     */
    private int x;

    /**
     * The y coordinate of the cell
     */
    private int y;

    /**
     * If the cell has been visited by the generation algorithm or not
     */
    private boolean isVisited = false;

    /**
     * If the cell is a wall in the maze
     */
    private boolean isWall = true;

    /**
     * If the cell is the entrance of the maze.  DO NOT have more than one
     * entrance
     */
    private boolean isEntrance = false;

    /**
     * If the cell is the exit of the maze.  DO NOT have more than one
     * exit
     */
    private boolean isExit = false;

    /**
     * Creates a MazeCell object
     *
     * @param x          The x location of the cell
     * @param y          The y location of the cell
     * @param parentMaze The maze holding this cell
     */
    public MyMazeCell(int x, int y, MyMaze parentMaze) {
        this.x = x;
        this.y = y;
        this.isVisited = false;
        this.isWall = true;
        this.parentMaze = parentMaze;
    }

    /**
     * Gets if there is an open cell next to this one
     *
     * @param parent Gets the cell neighboring checking this
     * @return If there is an open cell adjacent to this one
     */
    public boolean hasOpening(Point parent) {
        List<Point> adjacents = getAdjacents();
        for (Point p : adjacents) {
            if (!p.equals(parent)) {
                if (!parentMaze.isWall(p)){
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * Gets all possible adjacent points to this cell
     *
     * @return A list of adjacent points to this cell
     */
    private java.util.List<Point> getAdjacents() {
        List<Point> adjacents = new ArrayList<Point>();
        adjacents.add(new Point(x - 1, y));
        adjacents.add(new Point(x + 1, y));
        adjacents.add(new Point(x, y - 1));
        adjacents.add(new Point(x, y + 1));
        for (int i = adjacents.size() - 1; i >= 0; i--)
            if (!parentMaze.isInBounds(adjacents.get(i))
                    || adjacents.equals(new Point(x - 1, y)))
                adjacents.remove(i);
        return adjacents;
    }

    /**
     * Gets if this cell is the exit in the maze
     *
     * @return if this cell is an exit
     */
    public boolean isExit() {
        return isExit;
    }

    /**
     * Sets if this cell is an exit or not
     *
     * @param isExit if this cell should be an exit
     */
    public void setExit(boolean isExit) {
        this.isExit = isExit;
    }

    /**
     * Gets if this cell is the entrance to the maze
     *
     * @return if this cell is an entrance
     */
    public boolean isEntrance() {
        return isEntrance;
    }

    /**
     * Sets if this cell is an exit or not
     *
     * @param isEntrance if this cell should be an entrance
     */
    public void setEntrance(boolean isEntrance) {
        this.isEntrance = isEntrance;
    }

    /**
     * Gets if this cell is a wall. Wall cells cannot be walked through.
     *
     * @return if this cell is a wall
     */
    public boolean isWall() {
        return isWall;
    }

    /**
     * Sets if this cell should be a wall
     *
     * @param isWall if this cell should be a wall
     */
    public void setWall(boolean isWall) {
        this.isWall = isWall;
    }

    /**
     * Gets if this cell has been visited by the maze generation algorithm
     *
     * @return if the cell has been visited by the algorithm or not
     */
    public boolean isVisited() {
        return isVisited;
    }

    /**
     * Sets if this cell has been visited by the maze generation algorithm
     *
     * @param isVisited sets if this cell has been visited
     */
    public void setVisited(boolean isVisited) {
        this.isVisited = isVisited;
    }
}
