package cs151.mazegenerator.maze;

import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;

/**
 * Writes a maze.MyMaze object to a file
 *
 * @author Anthony Benavente
 * @version 2/8/14
 */
public class MyMazeWriter {

    /**
     * Symbol for a wall tile that Dr. K's maze creator can read
     */
    private final String WALL       = "W";

    /**
     * Symbol for a blank tile that Dr. K's maze creator can read
     */
    private final String BLANK      = " ";

    /**
     * Symbol for a entrance tile that Dr. K's maze creator can read
     */
    private final String ENTRANCE   = "E";

    /**
     * Symbol for a wall tile that Dr. K's maze creator can read
     */
    private final String EXIT       = "X";

    /**
     * Reference to the maze that this object is writing
     */
    private MyMaze maze;

    /**
     * Create a maze.MyMazeWriter object by passing in a maze
     *
     * @param maze The maze to write to a file
     */
    public MyMazeWriter(MyMaze maze) {
        this.maze = maze;
    }

    /**
     * Writes the maze to a given file name
     *
     * @param fileName The filename to write this maze to (does not have to be
     *                 a txt file but it would be nice)
     * @throws FileNotFoundException
     * @throws UnsupportedEncodingException
     */
    public void writeMaze(String fileName)
            throws FileNotFoundException,
                   UnsupportedEncodingException {
        PrintWriter writer = new PrintWriter(fileName, "UTF-8");

        // Write the size of the maze "<rows> <cols>"
        writer.println(maze.getHeight() + " " + maze.getWidth());

        // Write the maze to the file using proper symbols
        for (int y = 0; y < maze.getHeight(); y++) {
            for (int x = 0; x < maze.getWidth(); x++) {

                // Get the cell for the current row and column
                MyMazeCell cell = maze.getCell(x, y);

                if (cell.isWall()) {
                    // ------------------------------------
                    // Print "W" for a wall cell
                    // ------------------------------------
                    writer.print(WALL);
                } else {
                    // ------------------------------------
                    // Print "E" for entrance, "X" for exit,
                    // and " " for nothing
                    // ------------------------------------
                    writer.print(cell.isEntrance() ? ENTRANCE :
                                 cell.isExit()     ? EXIT     :
                                                     BLANK);
                }
            }
            writer.println();
        }

        writer.close();
    }

}
