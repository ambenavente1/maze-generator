package cs151.mazegenerator.maze;

/**
 * This is class is used for configuring the maze.MyMaze generator.  Eventually,
 * more options will be available such as algorithm type or complexity.
 *
 * @author Anthony Benavente
 * @version 2/8/14
 */
public class MyMazeConfig {

    /**
     * Width of the maze that is being created
     */
    private int width;

    /**
     * Height of the maze that is being created
     */
    private int height;

    /**
     * Creates a new maze config object used for options when creating a
     * maze. This constructor uses a default width and height of 0, 0.
     */
    public MyMazeConfig() {
        this(0, 0);
    }

    /**
     * Creates a new maze config object used for options when creating a
     * maze.
     *
     * @param width  the width of the configuration
     * @param height the width of the configuration
     */
    public MyMazeConfig(int width, int height) {
        this.width = width;
        this.height = height;
    }

    /**
     * The width of the maze that is being created with this class
     * (configuration)
     *
     * @return the width of the configuration
     */
    public int getWidth() {
        return width;
    }

    /**
     * Sets the width for this configuration
     *
     * @param width the new width of the configuration
     */
    public void setWidth(int width) {
        this.width = width;
    }

    /**
     * The height of the maze that is being created with this class
     * (configuration)
     *
     * @return the height of the configuration
     */
    public int getHeight() {
        return height;
    }

    /**
     * Sets the height for this configuration
     *
     * @param height the new height of the configuration
     */
    public void setHeight(int height) {
        this.height = height;
    }
}
