Maze Generator
==============

This project is a module built for the CS 151 assignment, Monkey Business.
Monkey Business involved a monkey that was programmable by the user, that
would try and get out of the maze. One of the requirements of the 
assignment was to build 2 of our own mazes and store them into .txt files,
so instead, I wrote a maze generator that would produce these files every
time the program ran. 

### Authors
  - Anthony Benavente